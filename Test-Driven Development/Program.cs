﻿using System;
using System.Collections.Generic;

namespace Test_Driven_Development
{
    class Program
    {
        static void Main(string[] args)
        {
            Business business = new Business();
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));
            business.AddJob(new Job(15));
            business.DoWork();

            Console.ReadLine();
        }
    }
}
