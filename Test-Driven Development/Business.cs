﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test_Driven_Development
{
    public class Business
    {
        public Business()
        {
            this.Employees = new List<Employee>();
            this.Jobs = new List<Job>();
        }

        public List<Employee> Employees { get; set; }

        public List<Job> Jobs { get; set; }

        public void AddEmployee(Employee employee)
        {
            // Add employee to the employee list.
        }

        public void AddJob(Job job)
        {
            // Add job to the job list
        }

        public void DoWork()
        {
            // For each job, if job isn't completed, check each employees to see if they have hours and have them DoWork(). If job is completed after work, break from loop.
        }
    }
}
