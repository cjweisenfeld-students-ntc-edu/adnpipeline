﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Test_Driven_Development
{
    class Program
    {
        static void Main(string[] args)
        {
            Business business = new Business();
            business.AddEmployee(new Employee(10, 10));
            business.AddEmployee(new Employee(10, 10));
            business.AddJob(new Job(15));
            business.DoWork();

            Console.ReadLine();

            SerializeBusiness(business);
        }

        public static void SerializeBusiness(Business business)
        {
            // Create a binary formatter
            BinaryFormatter formatter = new BinaryFormatter();

            // Create a file using the hard coded path and file name
            using (Stream stream = File.Create("C:\\ADN\\TestDriven.xml"))
            {
                // Serialize (save) the business to the root of C
                formatter.Serialize(stream, business);
            }
        }
    }
}
