﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileMoverService
{
    public partial class Service1 : ServiceBase
    {
        private static FileSystemWatcher fileSystemWatcher = new FileSystemWatcher();

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            EventLog eventLog = new EventLog("Application", ".", "EventLogEvents");
            eventLog.WriteEntry("The file mover service was started", EventLogEntryType.Information);

            fileSystemWatcher.Path = "C:\\ADN";
            fileSystemWatcher.EnableRaisingEvents = true;

            fileSystemWatcher.Created += OnCreate;
            fileSystemWatcher.Changed += OnChange;
            fileSystemWatcher.Renamed += OnRename;

            Console.ReadLine();

        }

        static void MoveFile(string sourcePath, string destinationPath)
        {
            if (Directory.Exists(fileSystemWatcher.Path))
            {
                Thread.Sleep(1000);
                if (File.Exists(sourcePath))
                {
                    if (File.Exists(destinationPath))
                    {
                        File.Delete(destinationPath);
                    }

                    File.Move(sourcePath, destinationPath);

                    Console.WriteLine($" The file has been moved from {sourcePath} to {destinationPath}");
                }
            }


        }

        static void OnChange(object sender, FileSystemEventArgs e)
        {
            try
            {
                MoveFile(e.FullPath, "C:\\ADNTarget\\" + e.Name);
            }
            catch
            {
                EventLog eventLog = new EventLog("Application", ".", "EventLogEvents");
                eventLog.WriteEntry("The file could not be moved", EventLogEntryType.Error);
            }

            Console.WriteLine($"File: {e.FullPath} {e.ChangeType}");
        }

        static void OnCreate(object sender, FileSystemEventArgs e)
        {
            try
            {
                MoveFile(e.FullPath, "C:\\ADNTarget\\" + e.Name);
            }
            catch
            {
                EventLog eventLog = new EventLog("Application", ".", "EventLogEvents");
                eventLog.WriteEntry("The file could not be moved", EventLogEntryType.Error);
            }

            Console.WriteLine($"File: {e.FullPath} {e.ChangeType}");
        }

        static void OnRename(object sender, FileSystemEventArgs e)
        {
            string source = e.FullPath;
            string dest = "C:\\ADNTarget\\" + e.Name;
            MoveFile(e.FullPath, dest);

            EventLog eventLog = new EventLog("Application", ".", "EventLogEvents");
            eventLog.WriteEntry("The file was moved", EventLogEntryType.Information);

            Console.WriteLine($"File: {e.FullPath} {e.ChangeType}");
        }
        protected override void OnStop()
        {
            EventLog eventLog = new EventLog("Application", ".", "EventLogEvents");
            eventLog.WriteEntry("The file mover service was stopped", EventLogEntryType.Information);
        }


    }
}
